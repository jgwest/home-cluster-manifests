#!/bin/bash

CURR_PWD=`pwd`

mkdir -p /tmp/prometh

cd /tmp/prometh

git clone https://github.com/prometheus-operator/kube-prometheus

cd kube-prometheus

cp -R manifests $CURR_PWD


