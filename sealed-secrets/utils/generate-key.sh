#!/bin/bash

# https://github.com/bitnami-labs/sealed-secrets/blob/main/docs/bring-your-own-certificates.md

export PRIVATEKEY="mytls.key"
export PUBLICKEY="mytls.crt"
#export NAMESPACE="sealed-secrets"
#export NAMESPACE="kube-system"
#export SECRETNAME="mycustomkeys"

openssl req -x509 -nodes -newkey rsa:4096 -keyout "$PRIVATEKEY" -out "$PUBLICKEY" -subj "/CN=sealed-secret/O=sealed-secret"
