#!/bin/bash

echo "Remove finalizer called on $1"

kubectl get Application/"$1" -n cluster-argocd -o=json | \
	jq '.metadata.finalizers = null' | kubectl apply -n cluster-argocd -f -

