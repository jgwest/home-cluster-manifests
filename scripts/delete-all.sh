#!/bin/bash


delete_application () {
	echo "* Deleting $1"
	sleep 60s && ./remove-finalizer.sh "$1" &
	SLEEP_PID=$!
	kubectl delete "application/$1" -n cluster-argocd 
	kill -9 $SLEEP_PID
}


ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"/..

set -x



delete_application sealed-secrets
# kubectl delete application/sealed-secrets -n cluster-argocd 

kubectl delete application/container-registry -n cluster-argocd 
kubectl delete application/staging-container-registry -n cluster-argocd 

kubectl delete application/kubernetes-dashboard -n cluster-argocd 
kubectl delete application/staging-kubernetes-dashboard -n cluster-argocd 


delete_application ingress-nginx
delete_application staging-ingress-nginx
#kubectl delete application/ingress-nginx -n cluster-argocd
#kubectl delete application/staging-ingress-nginx -n cluster-argocd

delete_application grafana-olm
delete_application staging-grafana-olm
#kubectl delete application/grafana-olm -n cluster-argocd
#kubectl delete application/staging-grafana-olm -n cluster-argocd

delete_application prometheus-olm
delete_application staging-prometheus-olm
#kubectl delete application/prometheus-olm -n cluster-argocd
#kubectl delete application/staging-prometheus-olm -n cluster-argocd

kubectl delete apiservices/v1.packages.operators.coreos.com
delete_application staging-olm

kubectl delete apiservices/v1.packages.operators.coreos.com
delete_application olm
#kubectl delete application/staging-olm -n cluster-argocd 
#kubectl delete application/olm -n cluster-argocd




kustomize build $ROOT/argo-cd/ | kubectl delete -f -

set -x

kubectl delete -n operators subscription/my-prometheus
kubectl delete -n staging-operators subscription/my-prometheus
kubectl delete -n grafana subscription/my-grafana-operator
kubectl delete -n staging-grafana subscription/my-grafana-operator

kubectl delete clusterserviceversions --all

kubectl delete apiservices/v1.packages.operators.coreos.com
kubectl delete namespace olm
kubectl delete apiservices/v1.packages.operators.coreos.com
kubectl delete namespace staging-olm

kubectl delete namespace operators
kubectl delete namespace staging-operators

kubectl delete catalogsources -A
kubectl delete installplans -A


kubectl delete -f $ROOT/../my-sealed-secret-key.yaml

kubectl delete ValidatingWebhookConfiguration/ingress-nginx-admission



kubectl delete namespace cluster-argocd
kubectl delete namespace kubernetes-dashboard
kubectl delete namespace nginx-ingress
kubectl delete namespace ingress-nginx-staging
kubectl delete namespace container-registry

kubectl delete namespace monitoring
kubectl delete namespace staging-monitoring

kubectl delete namespace grafana
kubectl delete namespace staging-grafana

kubectl delete clusterrole/staging-aggregate-olm-edit
kubectl delete clusterrole/staging-aggregate-olm-view
kubectl delete clusterrole/staging-system:controller:operator-lifecycle-manager
kubectl delete clusterrole/staging-global-operators-admin
kubectl delete clusterrole/staging-global-operators-edit
kubectl delete clusterrole/staging-global-operators-view
kubectl delete clusterrole/staging-olm-operators-admin
kubectl delete clusterrole/staging-olm-operators-edit
kubectl delete clusterrole/staging-olm-operators-view
kubectl delete clusterrole/staging-ingress-nginx
kubectl delete clusterrole/jgw-prometheus-kube-state-metrics
kubectl delete clusterrole/jgw-prometheus-kube-promet-operator
kubectl delete clusterrole/jgw-prometheus-kube-promet-prometheus-psp
kubectl delete clusterrole/psp-jgw-prometheus-prometheus-node-exporter
kubectl delete clusterrole/jgw-prometheus-kube-promet-operator-psp
kubectl delete clusterrole/jgw-prometheus-grafana-clusterrole
kubectl delete clusterrole/jgw-prometheus-kube-promet-prometheus
kubectl delete clusterrole/psp-jgw-prometheus-kube-state-metrics


kubectl delete clusterrole/ingress-nginx
kubectl delete clusterrole/ingress-nginx-admission
kubectl delete clusterrolebinding/ingress-nginx
kubectl delete clusterrolebinding/ingress-nginx-admission

kubectl delete validatingwebhookconfiguration/staging-ingress-nginx-admission
kubectl delete validatingwebhookconfiguration/ingress-nginx-admission
kubectl delete validatingwebhookconfiguration/my-nginx-ingress-nginx-admission


kubectl delete customresourcedefinition/sealedsecrets.bitnami.com
#kubectl delete customresourcedefinition/appprojects.argoproj.io
#kubectl delete customresourcedefinition/applications.argoproj.io
kubectl delete crd/argocdexports.argoproj.io                   
kubectl delete crd/operatorgroups.operators.coreos.com         
kubectl delete crd/catalogsources.operators.coreos.com         
kubectl delete crd/installplans.operators.coreos.com           
kubectl delete crd/operators.operators.coreos.com              
kubectl delete crd/subscriptions.operators.coreos.com          
kubectl delete crd/clusterserviceversions.operators.coreos.com 
kubectl delete crd/argocds.argoproj.io                         
kubectl delete crd/applicationsets.argoproj.io                 
kubectl delete crd/applications.argoproj.io                    
kubectl delete crd/appprojects.argoproj.io                     
kubectl delete crd/prometheusrules.monitoring.coreos.com       
kubectl delete crd/podmonitors.monitoring.coreos.com           
kubectl delete crd/probes.monitoring.coreos.com                
kubectl delete crd/servicemonitors.monitoring.coreos.com       
kubectl delete crd/alertmanagerconfigs.monitoring.coreos.com   
kubectl delete crd/alertmanagers.monitoring.coreos.com         
kubectl delete crd/thanosrulers.monitoring.coreos.com          
kubectl delete crd/prometheuses.monitoring.coreos.com          
kubectl delete crd/grafanadashboards.integreatly.org           
kubectl delete crd/grafanadatasources.integreatly.org          
kubectl delete crd/grafanas.integreatly.org                    


